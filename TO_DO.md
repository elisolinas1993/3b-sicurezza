# TODO-LIST

* Modalità d'uso dei cifrari a blocchi
* [Esponente modulare](esponente-modulare.tex)
  * Finire dimostrazione di correttezza
  * Calcolo complessità
* [Teoremi sui numeri primi](teoremi-numeri-primi.tex)
  * Piccolo teorema di Fermat
  * Principio fondamentale
* [Generazione numeri primi](generazione-numeri-primi.tex)
  * Dimostrazione di correttezza
  * Calcolo complessità
* [Paradosso del compleanno](paradosso-compleanno.tex)
  * Ricerca di una collisione generica
  * Ricerca di una collisione specifica