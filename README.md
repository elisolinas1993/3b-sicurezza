# 3B-Sicurezza

0. **Introduzione alla crittografia**
  * Tipologie di cifrari
    * Cifrari aperti VS cifrari chiusi
      * Cifrari aperti: l'algoritmo utilizzato per cifrare/decifrare è pubblico
      * Cifrari chiusi: l'algoritmo utilizzato per cifrare/decifrare è segreto/classificato
      * I cifrari moderni più utilizzati sono quelli aperti
        * Perché?
          * [Principio di Kerchoffs](https://en.wikipedia.org/wiki/Kerckhoffs%27s_principle)
          * Evitare [*"security through obscurity"*](https://en.wikipedia.org/wiki/Security_through_obscurity)
    * Cifrari simmetrici VS cifrari asimmetrici
      * [Cifrari simmetrici](https://en.wikipedia.org/wiki/Symmetric-key_algorithm): chiave utilizzata per cifrare è uguale alla chiave usata per decifrare.
        * [Cifrari a sostituzione](https://en.wikipedia.org/wiki/Substitution_cipher): un gruppo di caratteri viene sostituito con un altro gruppo di caratteri
        * [Cifrari a permutazione](https://en.wikipedia.org/wiki/Transposition_cipher): gruppi di caratteri vengono spostati nel testo
      * [Cifrari asimmetrici](https://en.wikipedia.org/wiki/Public-key_cryptography): chiave utilizzata per cifrare è diversa dalla chiave usata per decifrare
  * Definizioni di base
    * Plaintext (testo in chiaro): testo prima della encryption
    * Ciphertext (testo cifrato): testo dopo l'encryption

1. **Cifrari simmetrici classici**
  * Cifrari monoalfabetici a 1 lettera
    * **ES:** [Cifrario di Cesare](https://en.wikipedia.org/wiki/Caesar_cipher)
      * Schema di funzionamento  
        1. Si sceglie una chiave $K = \{0, ..., 20\}$
        2. Si numerano le lettere dell'alfabeto
        3. Per cifrare si sostituiscono tutte le occorrenze della lettera numero $X$ con la lettera numero $(X+K) \mod{21}$
        4. Per decifrare si sostituiscono tutte le occorrenze della lettera numero $X$ con la lettera numero $(X-K) \mod{21}$
      * Debolezza: il cifrario di Cesare è vulnerabile ad attacchi di forza bruta, visto che il numero di chiavi possibili è pari a 21
        * È sufficiente provare a decifrare il ciphertext con tutte le chiavi $K$ da $0$ a $20$ finché non si ottiene un testo di senso compiuto.
    * Schema generale del funzionamento di un cifrario monoalfabetico a $1$ lettera:
      1. Si sceglie una chiave $K = K_0 \dots K_{20}$ 
      2. Ciascun elemento $K_i$ della chiave individua una sostituzione arbitraria per una lettera dell'alfabeto
      3. Per cifrare si sostituiscono tutte le occorrenze della lettera numero $X$ con la lettera numero $(X+K_X) \mod{21}$
      4.  Per decifrare si sostituiscono tutte le occorrenze della lettera numero $X$ con la lettera numero $(X-K_X) \mod{21}$
    * Debolezze:
      * Attacchi di forza bruta diventano quasi impossibili perché, se l'alfabeto è costituito da $N$ lettere, il numero di chiavi è pari a $N!$ (*tutte le possibili permutazioni di $N$ lettere*)
        * Per 21 lettere si tratta di più di 51 miliardi di miliardi di chiavi
      * I cifrari monoalfabetici a una lettera sono vulnerabili ad attacchi di crittoanalisi statistica
        * Ciascuna lettera dell'alfabeto, in un dato linguaggio, compare con una certa frequenza 
        * Schema di attacco con crittanalisi statistica:
          1. Confrontare la frequenza delle lettere nel ciphertext con la frequenza delle lettere nella lingua del plaintext
          2. Ipotizzare possibili corrispondenze tra lettere del ciphertext e lettere del plaintext
          3. Verificare se il testo così parzialmente decifrato è possibile nella lingua del testo in chiaro, e, in caso contrario, annullare una parte delle corrispondenze ipotizzate
        * Più il ciphertext è lungo, più è facile decifrare
        * Se esistono parti di testo fisse (o probabili) è molto più facile decifrare
  * Cifrari monoalfabetici a $N$ lettere: ogni $N$-upla del testo in chiaro viene sostituita sempre dalla stessa sequenza di $N$ lettere nel testo cifrato
    * **ES:** [Cifrario di Playfair](https://en.wikipedia.org/wiki/Playfair_cipher)
      * Schema di funzionamento
        1. Si sceglie una parola chiave arbitraria, per esempio *"security"* e si costruisce una matrice 5x5 del tipo:

            | s   | e | c | u | r |
            |-----|---|---|---|---|
            | i/j | t | y | a | b |
            | d   | f | g | h | k |
            | l   | m | n | o | p |
            | q   | v | w | x | z |
        
         2. Si effettuano le seguenti sostituzioni:
           * Se sono presenti lettere ripetute si inserisce una lettera di riempimento
           * Le coppie di lettere che si trovano sulla stessa riga della tabella vengono sostituite con le lettere che le seguono a destra
           * Le coppie di lettere che si trovano sulla stessa colonna della tabella vengono sostituite con le lettere che le seguono in basso
           * Lettera[I, J], Lettera[K, M] -> Lettera[I, M], Lettera [K, J]  
    * Debolezze:
      * Il cifrario è migliore di quello per N=1, ma rimane comunque possibile una analisi statistica 
      * L’analisi è facile se il testo cifrato è lungo o se alcune parti del testo in chiaro sono note o probabili
  * [Cifrari polialfabetici](https://en.wikipedia.org/wiki/Polyalphabetic_cipher): una lettera o $N$-upla di lettere può essere cifrata diversamente (con trasformazioni alfabetiche diverse) a seconda della sua posizione nel testo
    * [Cifrario di Vigenère](https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher)
      * Schema di funzionamento:
        1. Selezionare una chiave $K = K_0 ... K_{n-1}$, dove ogni sottochiave $K_j$ è un numero tra 0 e 20
        2. Per cifrare, sostituire $T_j$ (la lettera in posizione $j$ del testo) con la lettera $(T_j + K_{j\%n}) \mod{21}$
          * Questo equivale ad applicare alla lettera $T_j$ il cifrario di Cesare corrispondente alla sottochiave $K_{J\%n}$ individuata dalla posizione $j$ di $T_j$ nel testo
      * Debolezza: se si conosce *n*, cioè la lunghezza della chiave, è possibile effettuare la stessa crittanalisi statistica dei cifrari monoalfabetici per lettere che distano *n* posizioni nel testo (per esse vale la stessa sostituzione)
  * Cifrari bit-oriented
    * [Cifrario di Vernam](https://en.wikipedia.org/wiki/One-time_pad)
      * Analogo al cifrario di Vigenère, ma applicato a un plaintext fatto di bit (sostituzione alfabetica -> $\oplus$) 
    * One-Time Pad
      * Un cifrario di Vigenère in cui la chiave ha la stessa lunghezza del plaintext
      * Unico cifrario inattaccabile, ma inutilizzabile: se esiste un modo sicuro per scambiarsi una chiave lunga quanto il messaggio, tanto vale scambiarsi direttamente il messaggio
  * [Cifrari a permutazione](https://en.wikipedia.org/wiki/Transposition_cipher)
    * Le lettere non vengono sostituite ma scambiate di posizione
      * **ES**: sistemare il testo su N colonne, scambiare le lettere invertendo le colonne secondo una permutazione segreta di N elementi
    * Debolezza: E’ possibile elaborare successivi raffinamenti di ipotesi di permutazione basandosi sulla frequenza di digrafi e trigrafi, o sulla presenza di testo fisso o probabile
    * Molto più sicuro se si usano più permutazioni in cascata con diverse chiavi, eventualmente combinandolo con tecniche di sostituzione
  * [Macchine a Rotori](https://en.wikipedia.org/wiki/Rotor_machine):
    * Costituite da $K$ rotori, ognuno dei quali individua una sostituzione monoalfabetica
    * Ogni rotore, girando, porta a una diversa sostituzione, ripetendosi dopo $N$ volte
    * Così otteniamo $N^K$ sostituzioni monoalfabetiche
    * Per ogni carattere del testo cambia la sostituzione monoalfabetica utilizzata

2. **Cifrari a blocchi**
  * [Il modello di Feistel](https://en.wikipedia.org/wiki/Feistel_cipher)
    ```mermaid
    graph LR
      
      A[Plaintext] --> B[L0]
      A --> C[R0]
      B --> D[XOR]
      E[K1] --> F[F]
      F --> D
      D --> G[R1]
      C --> H[L1]
    ```
    * Sia $C=E(P)$ e $P=D(C)$.
      1. Definire $E$
         * $E(L_0, R_0) = L_1, R_1 = R_0, (L_0 \oplus F(k_1, R_0))$
      2. Definire $D$
         * $D(L_1, R_1) = L_0, R_0 = (R_1 \oplus F(k_1, L_1)), L1$
      3. Dimostrare che $P = D(E(P))$
         * $D(E(L_0, R_0)) = D(R_0, (L_0 \oplus F(k_1,R_0)))$
         * $= (L_0 \oplus F(k_1, R_0)) \oplus F(k_1, L_1), R_0$
         * $= L_0 \oplus F(k_1, R_0) \oplus F(k_1, R_0) = L_0, R_0$
  * [DES](https://en.wikipedia.org/wiki/Data_Encryption_Standard)
    * [Principi di progettazione](https://en.wikipedia.org/wiki/Confusion_and_diffusion): sono quelli teorizzati da Shannon, secondo il quale un cifrario sicuro deve implementare 
      * Diffusione: la capacità dell'algoritmo di distribuire le correlazioni statistiche del testo lungo tutto l'alfabeto utilizzato dall'algoritmo di cifratura rendendo quanto più difficile possibile un attacco statistico
        * Questo si traduce nella presenza dell'*effetto valanga*: la variazione di un bit nel plaintext dovrebbe modificare in maniera *"pesante"* il testo in uscita
      * Confusione: la relazione tra la chiave e il testo cifrato sia quanto più complessa e non correlata possibile in modo tale che non si possa risalire ad essa a partire dal testo cifrato
    * Implementa il modello di Feistel con le seguenti caratteristiche:
      * Chiavi di 56 bit
      * 16 round
    * Schema di funzionamento
      ```mermaid
        graph LR

        A[Plaintext] -->|P1| B[Passo 1]
        B --> C[Passo 2]
        C --> D[Passo i]
        D --> E[Passo 16]
        E --> F[Swap]
        F --> G[PI^-1]
        G --> H[Ciphertext]

        I[Chiave] -->|P1| J[Shift]
        J -->|P2| K[K1]
        K --> B
        J --> L[Shift]
        L -->|P2| M[K2]
        M --> C
        L --> O[Shift]
        O -->|P2| P[Ki]
        P --> D
        P --> Q[Shift]
        Q -->|P2| R[K16]
        R --> E   
      ```
      * Note allo schema:
       1. $<K_1, ..., K_{16}>$ vengono ottenute mediante una *key schedule* e ciascuna di esse viene calcolata a partire dalla chiave originale
    * Schema di ciascun round
      ```mermaid
        graph LR

        A(L1) --> B[XOR]
        B --> C{R2}

        D(R1) --> E{L2}
        D --> F[E]
        F --> G[XOR]
        G --> H[S]
        H --> I[P]
        I --> B

        J[K2] --> B

      ```
      * I rombi contengono le due metà passo $1$
      * I rettangoli con gli angoli smussati contengono le due metà del passo $2$
    * Sicurezza del DES
      * A causa delle chiavi troppo brevi, è vulnerabile ad attacchi di forza bruta
      * Nel 1997 il DES viene violato in 5 mesi
      * Nel 1998 il DES viene violato in 39 giorni
      * Nel 1998 viene sviluppato il HW DES Cracker che viola il DES in 5 giorni
  * [Triple DES](https://en.wikipedia.org/wiki/Triple_DES)
    * L'idea di base è quella di migliorare la sicurezza del cifrario applicandolo più volte, ogni volta con una chiave diversa
      * *Perché il 3DES e non il 2DES?* Perchè il 2DES è soggetto ad attacchi *"meet-in-the-middle"*
        1. Supponiamo di conoscere una coppia $P_1, C_1$ (dove $P_1$ è il plaintext e $C_1$ il ciphertet) e di voler determinare la chiave $(K_1, K_2)$ utilizzata nella cifratura di $P_1$
        2. Cifriamo $P_1$ con tutte le $2^{56}$ possibili $K_1$ e memorizziamo i ciphertext $DES_{K_{1,i}}(P_1)$ ottenuti in una tabella
        3. Decifriamo $C_1$ con tutte le $2^{56}$ possibili $K_2$ e memorizziamo i plaintext $DES^{-1}_{K_{2,i}}(C_1)$ ottenuti in un'altra tabella
        4. A questo punto, per ottenere la coppia $(K_1, K_2)$ che porta da $P_1$ a $C_1$, è sufficiente confrontare le due tabelle e cercare la coppia $DES_{K_{1,i}}(P_1) = DES^{-1}_{K_{2,j}}(C_1)$
        5. A questo punto la chiave $(K_{1,i}, K_{2,j})$ potrebbe essere la chiave corretta, ma non è detto: *possono esistere diverse chiavi che dallo stesso plaintext generano lo stesso ciphertext*
            * In particolare, possono avvenire circa $2^{48}$ collisioni 
        6. Quindi si ripete la procedura con un'altra coppia di plaintext e ciphertext $(P_2, C_2)$ e si cerca una coppia di chiavi $(K_{1,i}, K_{2,j})$ tale che:
          $$DES_{K_{1,i}}(P_1) = DES^{-1}_{K_{2,j}}(C_1) \quad \wedge \quad DES_{K_{1,i}}(P_2) = DES^{-1}_{K_{2,j}}(C_2)$$
        7. A questo punto, la probabilità di determinare la chiave corretta è di circa $0.99$
    * Di solito viene utilizzato con $K_1 = K_3$, con la seconda fase che viene applicata in modalità *decryption* per fare in modo che il 3DES sia uguale al DES nel caso in cui si scelgano $K_1 = K_2 = K_3$
  * [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard)
    * Vengono utilizzate chiavi a 128/192/256 bit
    * Blocchi di 128 bit
    * Da 10 o 14 *rounds*
    * Efficiente
    * Progettato per macchine a 64 bit
    * Robusto rispetto ad attacchi lineari e differenziali
    * Unici attacchi noti sono di brute force
  * [Modalità d'uso dei cifrari a blocchi](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation)
    * Mentre gli algoritmi DES, AES, 3DES descrivono come un blocco viene cifrato/decifrato, le modalità d'uso dei cifrari a blocchi descrivono come cifrare/decifrare un plaintext composto da più blocchi
    * ECB (Electronic Code Book)
      ![ECB](pictures/ECB-encryption.svg)
      * In questo cifrario un blocco ripetuto viene cifrato nello stesso modo: è possibile effettuare crittanalisi statistica.
    * CBC(Cipher Block Chaining)
      ![CBC](pictures/CBC-encryption.svg)
      ![CBC](pictures/CBC-decryption.svg)
      * Problemi:
        1. Un errore di trasmissione di un solo bit rende impossibile decifrare il blocco corrispondente e quello immediatamente successivo.
        2. Prima di poter applicare il sistema al plaintext, quest'ultimo deve essere completamente disponibile (*possibile problema di efficienza)
          * **ES**: Criptare una partita trasmessa in diretta.
    * CFB (Cipher FeedBack)
      ![CFB](pictures/CFB-encryption.svg)
      ![CFB](pictures/CFB-decryption.svg)
      * Questa modalità permette di trasformare un cifrario a blocchi in un cifrario a flusso
      * È meno efficiente del CBC
      * Un errore di trasmissione di un bit nel testo cifrato si propaga per diversi blocchi: infatti, risulteranno indecifrabili il blocco successivo e altri blocchi che lo seguono
    * OFB (Output FeedBack)
      ![OFB](pictures/OFB-encryption.svg)
      ![OFB](pictures/OFB-decryption.svg)
      * Questa modalità permette di trasformare un cifrario a blocchi in un cifrario a flusso
      * È meno efficiente del CBC
      * Un errore di trasmissione di un bit rende indecifrabile solo il gruppo corrispondente, mentre il resto può essere decifrato normalmente

3. [Scambio di chiavi Diffie-Hellman](https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange)
  * *A cosa serve?*: per lo scambio di chiavi simmetriche
  * Schema di funzionamento:
    * Si sceglie un numero primo $q$ e una sua radice primitiva $\alpha$
    * $A$ calcola la sua chiave privata: $S_a < q$
    * $A$ calcola la sua chiave pubblica: $P_a = \alpha^{S_a} \mod{q}$
    * $B$ calcola la sua chiave privata: $S_b < q$
    * $B$ calcola la sua chiave pubblica: $P_b = \alpha^{S_b} \mod{q}$
    * La chiave condivisa è $K = P_b^{S_a} \mod{q} = (\alpha^{S_b})^{S_a} \mod{q} =(\alpha^{S_a})^{S_b} \mod{q}= P_a^{S_b} \mod{q}$
  * Calcolo dell'esponente modulare
    * Metodo ricorsivo
    * Metodo iterativo
  * Generazione di numeri primi
  * Generazione di una radice primitiva
  * Attacco *man-in-the-middle*: 
    * *"The Diffie-Hellman key exchange is vulnerable to a man-in-the-middle attack. In this attack, an opponent Carol intercepts Alice's public value and sends her own public value to Bob. When Bob transmits his public value, Carol substitutes it with her own and sends it to Alice. Carol and Alice thus agree on one shared key and Carol and Bob agree on another shared key. After this exchange, Carol simply decrypts any messages sent out by Alice or Bob, and then reads and possibly modifies them before re-encrypting with the appropriate key and transmitting them to the other party. This vulnerability is present because Diffie-Hellman key exchange does not authenticate the participants. Possible solutions include the use of digital signatures and other protocol variants."*
    * Soluzione: usare un canale autenticato per lo scambio di $P_a$ e $P_b$

4. [Cifrario RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem))
  * Schema di funzionamento:
    1. Si scelgono a caso due numeri primi $p$ e $q$ abbastanza grandi
    2. Si calcola il loro prodotto $n = pq$ (detto **modulo RSA**) e la funzione di Eulero di tale prodotto $\Phi(n) = (p-1) \cdot (q-1)$
    3. Si considera che la fattorizzazione di $n$ è segreta (*computazionalmente molto difficile da calcolare*)
    4. Si sceglie un numero $e$ (*esponente pubblico*), coprimo con $\Phi(n)$ e più piccolo di $\Phi(n)$
    5. Si calcola il numero $d$ (*esponente privato*), tale che $ed \equiv 1 \mod{\Phi(n)}$
    6. Il messaggio viene cifrato (usando la chiave pubblica $K^+ = <n,e>$) mediante l'operazione $m^e \mod{n} = c$
    7. Il messaggio viene decifrato (usando la chiave privata $K^- = <n,e>$) mediante l'operazione $c^d \mod{n} = m$
  * Generazione delle chiavi
  * Calcolo dell'inverso moltiplicativo
  * Dimostrazione di correttezza di RSA: sapendo che $ed \equiv 1 \mod{\Phi(n)} = ed \equiv 1 \mod{[(p-1) \cdot (q-1))]}$ dimostrare che $[(m^e) \mod{n}]^d \mod{n} = m$
  * Sicurezza
    * La sicurezza dei cifrari simmetrici è dovuta al fatto che l'avversario non conosce la trasformazione realizzata dal cifrario
    * La sicurezza dei cifrari asimmetrici, invece, è dovuta al fatto che la trasformazione è troppo difficile da calcolare a meno che non si sia in possesso dell'informazione segreta (*nel caso di RSA la fattorizzazione di $n$*) che viene utilizzata per cifrare/decifrare
  * *Perché se RSA è così sicuro si utilizzano ancora i cifrari a blocchi?*
    * Perché RSA è meno efficiente dei cifrari a blocchi
    * Si cifra la chiave simmetrica usando RSA e il messaggio usando un cifrario simmetrico (DES, 3DES o AES)

5. [Funzioni di hash](https://en.wikipedia.org/wiki/Cryptographic_hash_function)
  * Concetti base delle funzioni di hash
    * Una funzione di hash $H$ permette di trasformare un messaggio $m$ di lunghezza qualsiasi in un codice di lunghezza fissa $c$: $H(m) = c$
    * **Collisione**: si verifica quando, per due messaggi diversi $m_1$ e $m_2$ avviene che $H(m_1) = H(m_2)$ 
      * Collisioni $\Rightarrow$ problemi di sicurezza
  * [Attacco del compleanno](https://en.wikipedia.org/wiki/Birthday_attack): permette di scegliere $m_1$ ed $m_2$ tali che $m_1 \in M_1$ e $m_2 \in M_2$ dove $M_1$ e $M_2$ sono insiemi di messaggi predeterminati.
    * [*Paradosso del compleanno*](paradosso-compleanno.pdf): in un gruppo di 23 persone, la probabilità che almeno due di esse compiano gli anni nello stesso giorno è maggiore di 0.5
    * Come usare il paradosso del compleanno per attaccare una funzione di hash:
      1. Eva trova una variante $m_i$ di $M$ e una variante $m_j'$ di $M'$ tali che $<m_i, m_j'>$ sia una collisione per $H$
        * $m_i$ è una variante del messaggio iniziale accettabile per Alice
        * $m_j'$ è una variante del messaggio contraffatto accettabile per Eva
      2. Eva fa riautenticare $m_i$ ad Alice e usa l'autenticazione di $m_i$ per inviare $m_j'$
  * Una funzione di hash dovrebbe essere:
    * *Non invertibile*: dato $c$ è difficile trovare $m$ tale che $H(m) = c$
    * *Fortemente non invertibile*: dato $m_1$ è difficile trovare $m_2$ tale che $H(m_1) = H(m_2)$
    * *Resistente alle collisioni*: è difficile trovare $m_1$ e $m_2$ tali che $H(m_1) = H(m_2)$
    * **N.B.:** una funzione resistente alle collisioni è anche fortemente non invertibile, e una funzione fortemente non invertibile è anche non invertibile.
  * Esempi di funzioni di hash:
    * [MD5](https://en.wikipedia.org/wiki/MD5)
    * [SHA-1](https://en.wikipedia.org/wiki/SHA-1)

6. **Autenticazione**
  * *Perché un messaggio cifrato non è necessariamente autentico?*
    * Se $A$ e $B$ condividono una chiave segreta, e $A$ invia a $B$ un messaggio cifrato, $B$ dovrebbe essere sicuro che il messaggio provenga effettivamente da $A$, visto che è l'unico a possedere la chiave
    * In realtà, un avversario $E$ potrebbe intercettare la comunicazione e sostituire il messaggio prodotto da $A$ con un altro
    * Il messaggio inviato da $E$, probabilmente risulterà incomprensibile una volta decifrato da $B$, ma non nel caso in cui si tratti di numeri o codici
  * [Autenticazione simmetrica](https://en.wikipedia.org/wiki/Message_authentication_code)
    * [MAC con DES-CBC](https://en.wikipedia.org/wiki/CBC-MAC)
    * MAC con funzione di hash
      * [HMAC](https://en.wikipedia.org/wiki/HMAC)
  * [Firma elettronica](https://en.wikipedia.org/wiki/Digital_signature): 
    * RSA/SHA-1
    * Problema di distribuzione delle chiavi pubbliche
      * Firma elettronica con [certificato di chiave pubblica](https://en.wikipedia.org/wiki/Public_key_certificate) 
      * Firma elettronica con certificato e [timestamp](https://en.wikipedia.org/wiki/Trusted_timestamping)

7. **Sicurezza delle LAN**
  * Falsificazione indirizzi
    * [MAC Spoofing](https://en.wikipedia.org/wiki/MAC_spoofing)
      * [ARP Poisoning](https://en.wikipedia.org/wiki/ARP_spoofing)
    * [IP Spoofing](https://it.wikipedia.org/wiki/IP_spoofing)
    * [DNS Spoofing](https://en.wikipedia.org/wiki/DNS_spoofing)
    * [URL Spoofing](https://en.wikipedia.org/wiki/Spoofed_URL)
  * [DoS (Denial of Service)](https://en.wikipedia.org/wiki/Denial-of-service_attack): si bombarda un server di richieste in modo da sovraccaricarlo e di impedirgli di funzionare correttamente
    * [Smurf attack](https://en.wikipedia.org/wiki/Smurf_attack) 
      1. Si invia un gran numero di richieste ICMP `ECHO_REQUEST` (*ping*), con indirizzo mittente falsificato (mediante IP spoofing) e corrispondente all'indirizzo IP del "computer vittima" (target), a una serie di gateway
      2. Il gateway malconfigurato riceve il ping e lo instrada a tutti i nodi connessi alla propria sottorete, i quali invieranno un pacchetto di risposta ECHO_REPLY (tipo 0) all'indirizzo IP mittente
        * In una rete broadcast a multiaccesso (come ad esempio reti di provider) centinaia di macchine connesse potrebbero rispondere ad ogni pacchetto
        * Gli indirizzi destinati al gateway generalmente sono quelli con la porzione dedicata all'host con i bit tutti ad 1
          *  Ad esempio, l'indirizzo IP di broadcast della rete 10.0.0.0 è 10.255.255.255.
      3. Generalmente l'attacco smurf risulta efficace utilizzando numerosi gateway. I fautori dell'attacco necessitano quindi di broadcast list (smurf amplifier), generalmente file di testo contenenti una lista di indirizzi IP corrispondenti a gateway malconfigurati presenti in rete
        * Questo attacco, a causa del fatto che "amplifica" la quantità di dati usati dall'attaccante, fa parte dei cosiddetti amplification attack
      4. Tale tecnica consente di generare molto traffico, soprattutto considerando che i messaggi ICMP sono incapsulati in un datagram IP, che consente l'inserimento di dati opzionali, e nel caso di richieste eco (ECHO_REQUEST), tali dati saranno replicati nei messaggi di risposta
      5. Il nodo vittima (target) riceverà un flood di pacchetti ICMP da numerosi indirizzi IP 
      6. Questo attacco causa:
        * Saturazione della banda disponibile e quindi disconnessione delle connessioni TCP
        * Disconnessione della rete in connessioni analogiche
        * Crash dei firewall software
        * Crash di sistemi operativi obsoleti
    * [Syn Flooding](https://en.wikipedia.org/wiki/SYN_flood)

8. [Firewall](https://en.wikipedia.org/wiki/Firewall_(computing))
  * Cos'è e a cosa serve:
    * É un dispositivo (software o hardware) che permette di filtrare il traffico in entrata/uscita da una LAN privata a seconda dei criteri specificati nella *firewall policy* da un amministratore
    * Produce un log del traffico complessivo e delle azioni dei singoli utenti
    * In caso di problemi, può generare allarmi
  * Configurazione
    * Screening router
    * Dual-homed gateway
    * Screened host gateway
    * Screened subnet
    * Altri comonenti che possono essere presenti nella configurazione:
      * DMZ
      * Extranet
      * HA (High Availability)
    * Esempi concreti    
      * Reti con LAN e DMZ
      * Reti con LAN, DMZ ed Extranet
      * Reti con LAN, DMZ + HA
      * Reti con LAN, DMZ, HA ed Extranet
  * Tipologie di Firewall
    * Packet filter
      * Come funziona: è un router che filtra pacchetti
      * Si può impostare in una delle seguenti modalità:
        * Default-deny: impedisce il passaggio di tutti i pacchetti tranne quelli esplicitamente permessi
        * Default-allow: permette il passaggio di tutti i pacchetti tranne quelli esplicitamente vietati
        * **MEGLIO DEFAULT-DENY**
      * Criteri di filtering:
        * Direzione del pacchetto (da/verso l'esterno)
        * Direzione della connessione TCP (da/verso l'esterno)
        * Indirizzo IP sorgente e/o destinazione
      * Cosa filtrare
        * Porte note
          * Meglio permettere: dati e controllo FTP, telnet, SMTP, DNS, HTTP, POP3
          * Meglio bloccare: whois, bootp, tftp, finger, SNMP, exec, talk, uucp
        * Frammentazione IP: se un pacchetto viene frammentato in pezzi molto piccoli, ogni parte può essere tanto ridotta da non includere neanche l'header TCP e quindi la porta utilizzata nel firewall per filtrare
          * Questo succede per frammenti di poco più di 20 byte, che sono comunque ingiustificati rispetto a qualsiasi MTU
          * Tali frammenti corti devono quindi essere tagliati
        * [Source Routing](https://en.wikipedia.org/wiki/Source_routing)
      * Access Control List: contiene l'elenco di regole che vengono controllate sequenzialmente.
        * [Esempi di ACL](esempi-acl.pdf)
      * Limitazioni del firewall packet filter
        * Difficoltà nella gestione di FTP
          * Ricordiamo come funziona FTP
            * FTP è un protocollo client-server che permette la trasmissione di dati tra due host.
            * Vengono usate due connessioni TCP distinte:
              * Una per controllare i trasferimenti
              * Una per trasferire i dati
            * Il client si connette alla porta 21 del server per iniziare la connessione
            * Il server risponde al client indicandogli una porta *effimera* a cui connettersi per effettuare il trasferimento
          * Il problema del firewall è questo: cosa inseriamo come flag nella regola che permette di gestire il traffico sulla connessione del trasferimento?
            * Se mettiamo ACK, blocchiamo il funzionamento di FTP
            * Se non mettiamo ACK, permettiamo a chiunque di aprire una connessione con noi sulla porta specificata   
        * Non selettivo rispetto agli utenti
        * Non mantiene log
        * Difficile monitorare gli attacchi mentre avvengono
      * Vantaggi del firewall packet filter:
        * Trasparenza: non è necessario apportare alcuna modifica alle applicazioni
        * Costi: è una soluzione molto economica
        * Efficienza
    * [Firewall applicativo](https://en.wikipedia.org/wiki/Application_firewall)
      * Si utilizza un proxy, cioè un server che funge da intermediario per le richieste da parte dei client verso i server.
      * Come funziona: è un calcolatore attraverso cui il traffico passa e viene filtrato/registrato a livello applicativo
        * Sono proibite le connessioni dirette tra l'esterno e la rete interna (questo può essere realizzato semplicemente mediante una regola su packet filter)
        * Sono possibili soltanto le connessioni attraverso il firewall
      * Proxy applicativo
      * Packet filter application aware
      * Transparent Proxy
        * Il transparent proxy è un proxy che intercetta la comunicazione a livello di rete, senza richiedere alcuna configurazione da parte del client.
      * Limitazioni del firewall di livello applicativo
        * Non trasparente
        * Richiede un host dedicato
        * Prestazioni medie
      * Vantaggi del firewall di livello applicativo
        * Sicuro
        * Permette di riferire il traffico agli utenti
        * Mantiene log sofisticati
      * [NAT (Network Address Translation)](https://en.wikipedia.org/wiki/Network_address_translation): Permette di fornire all'esterno degli indirizzi IP diversi da quelli reali delle macchine presenti nella LAN
      * NAPT (Network Address Port Translation)
        * Permette di fornire all'esterno degli indirizzi IP e delle porte TCP diversi da quelli reali delle macchine presenti nella LAN
  * Vulnerabilità dei firewall
    * Non evita attacchi dall'interno del sistema
    * Non evita problemi di sicurezza sui servizi e sui protocolli aperti verso l'esterno
    * Non evita il DoS (Denial of Service)

9. [VPN (Virtual Private Network)](https://en.wikipedia.org/wiki/Virtual_private_network)
  * VPN: permette di creare una *LAN aziendale virtuale* che interconnetta sistemi sistemi interni all'azienda stessa dislocati su un ampio territorio
    * **ES:** Per mezzo di una VPN, utilizzando una connessione Internet, è  possibile collegarsi da remoto (cioè dall'esterno) alla rete informatica della propria azienda
      * La connessione si svolge attraverso un tunnel "virtuale" (protetto e sicuro) supportato da Internet esattamente come fosse il cavo fisico abituale
    * Le VPN sicure utilizzano protocolli crittografici a tunnel per offrire l'autenticazione del mittente e l'integrità del messaggio allo scopo di difendere la privacy.
      * Una volta scelte, implementate e usate, alcune tecniche possono fornire comunicazioni sicure su reti non sicure. Il protocollo più usato per creare una VPN sicura è:
        * [IPsec (IP security)](https://en.wikipedia.org/wiki/IPsec): permette di cifrare e/o autenticare la PDU (Protocol Data Unit) di un datagramma IP
          * Modalità di funzionamento
            * Modalità transport:
              * Connessione host-to-host
              * Viene usato dagli endpoint e non dai gateway
                * Gateway: dispositivo di rete che collega due reti informatiche di tipo diverso operando sia al livello di rete sia ai livelli superiori, del modello ISO/OSI. Il suo scopo principale è quello di veicolare i pacchetti di rete all'esterno di una LAN.
                * Gateway è un termine generico che indica il servizio di inoltro dei pacchetti verso l'esterno; il dispositivo hardware che porterà a termine questo compito è tipicamente un router. Nelle reti più semplici è presente un solo gateway che inoltra tutto il traffico diretto all'esterno verso la rete Internet. In reti più complesse in cui sono presenti parecchie subnet, ognuna di queste fa riferimento ad un gateway che si occuperà di instradare il traffico dati verso le altre sottoreti o reindirizzarlo ad altri gateway. 
                * Spesso i gateway non si limitano a fornire la funzionalità di base di routing, ma integrano altri servizi da e verso la rete locale come proxy DNS, firewall, NAT etc, che sono appunto servizi di strato di rete più elevato ovvero applicativo.
                * Funzionamento gateway:
                  * Un computer connesso alla rete locale confronta i primi bit dell'indirizzo di destinazione dei dati da inviare (quelli che corrispondono ai bit settati a "1" nella sua subnet mask) con il network prefix (già noto) del proprio indirizzo IP.
                    * Se corrispondono, significa che il computer di destinazione è sulla stessa rete locale.
                    * Se invece non corrispondono, il computer d'origine invia i dati al gateway predefinito,il quale si occuperà del loro successivo instradamento verso la rete remota di destinazione.
                    * Non possono coesistere in una stessa rete 2 computer con lo stesso indirizzo IP (conflitto IP; il secondo arrivato disattiva la propria scheda di rete).
              * In caso di cifratura, viene cifrato solo il payload del datagramma IP e non l'header
              * Computazionalmente leggero
              * Ogni host che vuole comunicare deve avere tutto il software necessario ad implementare IPsec
              * Si aggiunge solo l'header IPsec: gli indirizzi mittente e destinatario degli endpoint sono leggibili
            * Modalità tunnel:
              * Connessione gateway-to-gateway
              * In caso di cifratura, viene cifrato tutto il pacchetto IP originale
              * Computazionalmente oneroso
              * Solo i gateway devono avere il software necessario a implementare IPsec
              * Esistono dei punti di centralizzazione (che costituiscono dei *single point of failure*)
              * Utilizza un oppio incapsulamento, ponendo come payload della comunicazione tra indirizzi gateway quanto si ottiene cifrando l'unione di indirizzi mittente e destinatario degli end-point col payload effettivo; adottando il protocollo Encapsulating Security Payload, gli indirizzi mittente e destinatario degli end-point non sono quindi più rilevabili (restano invece rilevabili adottando AH).
              * **Di solito questa è la modalità usata per le VPN**
        * Protocolli IPsec:
            * AH (Authentication Header): autentica la PDU. 
              * Tunnel mode
              * Transport mode
            * ESP (Encapsulating Security Payload): cifra la PDU
              * Tunnel mode
              * Transport mode

10. **Sicurezza Web Applications**
  * [OWASP Top-Ten](https://www.owasp.org/index.php/Top_10_2010-Main)
    * [Injection](https://www.owasp.org/index.php/Top_10_2010-A1)
    * [XSS (Cross Site Scripting)](https://www.owasp.org/index.php/Top_10_2010-A2)
      * Si tratta di un tipo particolare di injection, in cui gli script malevoli vengono iniettati all'interno di siti web altrimenti legittimi e fidati
      * L'avversario utilizza una web app per inviare del codice malevolo (*solitamente sotto forma di uno script client-side*) ad un altro utente
      * I problemi che permettono questo tipo di attacchi sono causati principalmente dal fatto che una web app utilizza i dati in input inviati dall'utente per generare l'output senza validarli o controllarli
      * Poiché lo script arriva da un sito ritenuto affidabile, il browser della vittima lo esegue, dandogli l'accesso ai cookies, token di sessione o altre informazioni sensibili mantenuti dal browser e usati su quel sito
    * [Broken Authentication & Session Management](https://www.owasp.org/index.php/Top_10_2010-A3)
    * [Insecure Direct Object References](https://www.owasp.org/index.php/Top_10_2010-A4) 
    * [CSRF (Cross Site Request Forgery)](https://www.owasp.org/index.php/Top_10_2010-A5)
    * [Security misconfiguration](https://www.owasp.org/index.php/Top_10_2010-A6)
    * [Insecure cryptographic storage](https://www.owasp.org/index.php/Top_10_2010-A7)
    * [Failure to restrict URL access](https://www.owasp.org/index.php/Top_10_2010-A8)
    * [Insufficient transport layer protection](https://www.owasp.org/index.php/Top_10_2010-A9) 
    * [Unvalidated redirects and forwards](https://www.owasp.org/index.php/Top_10_2010-A10)

11. **Risk Management**
  * [ISO 27001](https://it.wikipedia.org/wiki/ISO/IEC_27001)
  * [Metodologia OWASP](https://www.owasp.org/index.php/OWASP_Risk_Rating_Methodology)

12. **Bitcoin e Blockchain**
  * [Concetti di base](https://medium.com/@zhaohuabing/hash-pointers-and-data-structures-f85d5fe91659)
    * Funzioni di hash *"one-way"*
    * Hash pointers
      * Cosa sono: una struttura dati costituita da due informazioni:
        * Puntatore alla locazione in memoria di una data informazione
        * Hash dell'informazione a cui il puntatore fa riferimento
      * Notazione
      * Proprietà
    * [Hash chains](https://en.wikipedia.org/wiki/Hash_chain)
      * Cosa sono: una catena di blocchi di dati, ciascuno dei quali contiene:
        * Un hash pointer al blocco precedente (sia dati che hash pointer)
        * Un nuovo blocco di dati
      * Notazione
      * Aggiunta di un elemento
      * Proprietà
      * Hash chains firmate
    * Chiavi pubbliche usate come identità
  * [Bitcoin](https://en.wikipedia.org/wiki/Bitcoin)
    * Bitcoin come dati firmati
        * Esempi di pagamento
        * Esempio di doppia spesa
    * Sistema di Bitcoin centralizzato
        * Hash chain di pagamenti
        * Ottimizzazioni
          * Più pagamenti per blocco
          * Assegnazione di un ID, tipo e firma alle transazioni
            * Un certo numero di coins vengono usati e creati all'interno di ciascuna transazione
            * Tipi di transazione:
              * Creazione di coins
              * Pagamento mediante coins
    * [Blockchain](https://en.wikipedia.org/wiki/Blockchain)
      * Principi:
        * Sostituzione dell'autorità centrale con una rete P2P
        * La validità della blockchain viene determinata dai nodi della rete 
        * La firma viene sostituita da una *"Proof of work"*
        * La creazione dei coin è contenuta  nell'operazione di aggiunta di un blocco
        * Per invitare i nodi della rete a validare la blockchain, si utilizza un incentivo economico
    * Come avviene la crescita della blockchain
    * Come viene evitata la doppia spesa
    * Creazione di bitcoin
    * Ottimizzazione della blockchain
      * Utilizzo di un Merklee Tree
        * Proof of membership
        * Proof of non-membership

13. **Sicurezza dei calcolatori**
  * [Attacco BOF (Buffer OverFlow)](https://en.wikipedia.org/wiki/Buffer_overflow)
    * Un buffer overflow é un bug che si verifica quando dati copiati in una locazione di memoria eccedono la grandezza riservata per tale variabile.
      * Quando si verifica un overflow, i dati in eccesso vengono copiati nelle locazioni di memoria adiacenti.
    * **Esempi di attacchi BoF**
      1. Semplice Buffer Overflow
      2. Riscrittura EIP usando codice già esistente
      3. Riscrittura EIP usando codice inserito in una variabile d'ambiente
      4. Riscrittura EIP usando codice inserito in una variabile d'ambiente (*Shellcode*)
      5. Ottenere l'accesso a una root shell con l'attacco descritto al punto 4
  * [Malware](https://en.wikipedia.org/wiki/Malware)
    * [Virus](https://en.wikipedia.org/wiki/Computer_virus)
      * Virus di prima generazione
      * [Virus polimorfi](https://en.wikipedia.org/wiki/Computer_virus#Polymorphic_code)
      * [Come funzionano gli antivirus](https://en.wikipedia.org/wiki/Antivirus_software)
    * [Worm](https://en.wikipedia.org/wiki/Computer_worm)
    * [Trojan Horse](https://en.wikipedia.org/wiki/Trojan_horse_(computing))